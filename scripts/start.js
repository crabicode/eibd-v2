const { Connection } = require('../');


const con = new Connection({
  host: '172.16.231.45',
  port: 3671,
});

process.on('unhandledRejection', (err) => {
  throw err;
});

process.on('SIGINT', async () => {
  try {
    await con.close();
  } catch (err) {
    console.log(err);
  }
});

con.on('message', console.log);

async function main() {
  con.write(5, 1.001, 1);
  // await con.open();
  // try {
  // } catch (err) {
  //   console.log(err);
  // } finally {
  //   // await con.close();
  // }
}

main();
