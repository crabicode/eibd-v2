const GroupAddress = require('./group-address');
const Exception = require('./exception');

const inRange = (a, b, c) => a <= b && b <= c;

class GroupAddress3 extends GroupAddress {
  /**
   *
   * @param {number | string} address
   */
  constructor(address, allowZero = false) {
    super(address);
    if (typeof address === 'number') {
      if (!inRange(allowZero ? 0 : 1, address, 0xffff)) {
        throw new Exception('group address is out of range');
      }
      this.value = address;
      return;
    }
    const match = address.match(/^(\d+)\/(\d+)\/(\d+)$/);
    const [, aStr, bStr, cStr] = match;
    const a = parseInt(aStr, 10);
    const b = parseInt(bStr, 10);
    const c = parseInt(cStr, 10);
    if (!(inRange(0, a, 0x1f) && inRange(0, b, 0x07) && inRange(0, c, 0xff))) {
      throw new Exception('Group address is invalid');
    }
    this.value = ((a & 0x1f) << 11) | ((b & 0x07) << 8) | ((c & 0xff));
    if (!allowZero && this.value === 0) {
      throw new Exception('Group address with zero value is not allowed');
    }
  }

  toString() {
    return [
      (this.value >> 11) & 0x1f,
      (this.value >> 8) & 0x07,
      (this.value) & 0xff,
    ].join('/');
  }

  get bytes() {
    const bytes = Buffer.allocUnsafe(2);
    bytes.writeUInt16BE(this.value);
    return bytes;
  }
}

module.exports = {
  unsafe(address, allowZero = false) {
    try {
      return this.new(address, allowZero);
    } catch (caughtErr) {
      return null;
    }
  },
  new(address, allowZero = false) {
    const addr = new GroupAddress3(address, allowZero);
    Object.freeze(addr);
    return addr;
  },
};
