const Listener = require('./listener');
const MessageTypes = require('./message-types');
const GroupAddress = require('./group-address-3');
const IndividualAddress = require('./individual-address');
const utils = require('./utils');
const Exception = require('./exception');

class MessageHandler extends Listener {
  constructor(...args) {
    super(...args);
    this.on('open', () => {
      this.socket.on('message', this.handleMessage.bind(this));
    });
  }

  async wait(event, callback, timeout = this.config.timeout) {
    if (typeof callback !== 'function') {
      throw new Exception('Expected callback to be a function');
    }
    if (!event) {
      throw new Exception('Expected event to be a string');
    }
    let tid = null;
    let resHandler = null;
    let closeHandler = null;
    const closeEvent = 'close';
    const release = () => {
      if (closeHandler) {
        this.removeListener(closeEvent, closeHandler);
      }
      if (tid) {
        clearTimeout(tid);
      }
      if (resHandler) {
        this.removeListener(event, resHandler);
      }
    };
    return new Promise(async (resolve, reject) => {
      closeHandler = (err) => {
        const closeErr = err || new Exception('Connection closed');
        this.emit('error', closeErr);
        reject(closeErr);
        release();
      };
      resHandler = (resErr, res) => {
        if (tid) {
          if (resErr) {
            this.emit('error', resErr);
            reject(resErr);
            release();
            return;
          }
          resolve(res);
          release();
        }
      };
      try {
        const isSubscribed =
          this.on(event, resHandler) &&
          this.on(closeEvent, closeHandler);
        if (isSubscribed) {
          if (this[callback.name] === callback) {
            await callback.call(this);
          } else {
            await callback();
          }
          tid = setTimeout(() => {
            const { name } = callback;
            const timeoutErr = new Exception(callback.name ?
              `request ${name} timeout` :
              'request timeout');
            this.emit('error', timeoutErr);
            reject(timeoutErr);
            release();
          }, timeout);
          return;
        }
        throw new Exception('Failed to subscribe to internal events');
      } catch (caughtErr) {
        reject(caughtErr);
        release();
      }
    });
  }

  handleMessageType(bytes) {
    switch (bytes[2]) {
      case (0x02): {
        switch (bytes[3]) {
          case (0x06):
            return MessageTypes.CONNECT_RESPONSE;
          case (0x09):
            return MessageTypes.DISCONNECT_REQUEST;
          case (0x0a):
            return MessageTypes.DISCONNECT_RESPONSE;
          case (0x08):
            return MessageTypes.CONNECTIONSTATE_RESPONSE;
          default:
            return MessageTypes.UNKNOWN;
        }
      }
      case (0x04): {
        switch (bytes[3]) {
          case (0x20):
            return MessageTypes.TUNNELLING_REQUEST;
          case (0x21):
            return MessageTypes.TUNNELLING_ACK;
          default:
            return MessageTypes.UNKNOWN;
        }
      }
      default:
        return MessageTypes.UNKNOWN;
    }
  }

  handleMessage(message, sender) {
    try {
      this.emit('raw', message, sender);
      const messageType = this.handleMessageType(message);
      switch (messageType) {
        case MessageTypes.CONNECT_RESPONSE: {
          const result = this.handleConnection(message, sender);
          this.emit('connecting', null, result, sender);
          this.emit('message', null, result, sender);
          return result;
        }
        case MessageTypes.DISCONNECT_RESPONSE: {
          const result = this.handleDisconnect(message, sender);
          this.emit('disconnecting', null, result, sender);
          this.emit('message', null, result, sender);
          return result;
        }
        case MessageTypes.TUNNELLING_REQUEST: {
          const result = this.handleRequest(message, sender);
          switch (result.apci) {
            case 0x00:
              this.emit(`read.${result.dst.value}`, null, result, sender);
              this.emit('message', null, result, sender);
              break;
            case 0x01:
            case 0x02:
              this.emit(`write.${result.dst.value}`, null, result, sender);
              this.emit('message', null, result, sender);
              break;
            default:
              break;
          }
          return result;
        }
        case MessageTypes.TUNNELLING_ACK: {
          const result = this.handleAck(message, sender);
          this.emit(`ack.${result.seqn}`, null, result, sender);
          this.emit('message', null, result, sender);
          return result;
        }
        case MessageTypes.CONNECTIONSTATE_RESPONSE: {
          const result = this.handleState(message, sender);
          this.emit('state', null, result, sender);
          this.emit('message', null, result, sender);
          return result;
        }
        default:
          this.emit('unhandled', null, message, sender);
          return null;
      }
    } catch (caughtErr) {
      utils.isSafeError(caughtErr);
      this.emit('unprocessed', caughtErr, message, sender);
      return null;
    }
  }

  handleRequest(bytes) {
    const message = Object.create(null);
    message.header |= bytes[0];
    message.version |= bytes[1];
    message.sid |= bytes[2] << 8;
    message.sid |= bytes[3];
    message.length |= bytes[4] << 8;
    message.length |= bytes[5];
    message.structureLength |= bytes[6];
    message.channel |= bytes[7];
    message.seqn |= bytes[8];

    this.ack(message.seqn);

    message.reserved |= bytes[9];
    message.code |= bytes[10];

    message.additionalInfoLength |= bytes[11];
    message.controlField1 |= bytes[12];
    message.controlField2 |= bytes[13];
    message.src |= bytes[14] << 8;
    message.src |= bytes[15] << 0;
    message.src = IndividualAddress.new(message.src);
    message.dst |= bytes[16] << 8;
    message.dst |= bytes[17] << 0;
    message.dst = GroupAddress.new(message.dst);

    if (message.code !== 0x29) {
      Object.freeze(message);
      return message;
    }

    message.npdu |= bytes[18];
    message.tpci |= bytes[19] & 0xC0;
    message.apci |= bytes[19];
    message.apci |= bytes[20];
    message.apci = (message.apci & 0x3C0) >> 6;

    switch (message.apci) {
      case 0x00:
        message.cemi = Buffer.allocUnsafe(0);
        break;
      case 0x01:
      case 0x02:
        // eslint-disable-next-line no-restricted-syntax
        if (message.npdu === 1) {
          if (bytes[0] < 0x3f) {
            const value = bytes[20] & 0x3f;
            message.cemi = Buffer.from([value]);
            break;
          }
          const value = bytes[21];
          message.cemi = Buffer.from([value]);
          break;
        }
        message.cemi = bytes.slice(21);
        break;
      default:
        break;
    }

    Object.freeze(message);
    return message;
  }

  handleAck(bytes) {
    const message = Object.create(null);
    message.header |= bytes[0];
    message.version |= bytes[1];
    message.sid |= bytes[2] << 8;
    message.sid |= bytes[3];
    message.length |= bytes[4] << 8;
    message.length |= bytes[5];
    message.channel |= bytes[7];
    message.seqn |= bytes[8];
    message.status |= bytes[9];
    Object.freeze(message);
    return message;
  }

  handleDisconnect(bytes) {
    const message = Object.create(null);
    message.header |= bytes[0];
    message.version |= bytes[1];
    message.sid |= bytes[2] << 8;
    message.sid |= bytes[3];
    message.length |= bytes[4] << 8;
    message.length |= bytes[5];
    message.channel |= bytes[6];
    message.status |= bytes[7];
    Object.freeze(message);
    return message;
  }

  handleState(bytes) {
    const message = Object.create(null);
    message.header |= bytes[0];
    message.version |= bytes[1];
    message.sid |= bytes[2] << 8;
    message.sid |= bytes[3];
    message.length |= bytes[4] << 8;
    message.length |= bytes[5];
    message.channel |= bytes[6];
    message.status |= bytes[7];
    Object.freeze(message);
    return message;
  }

  handleConnection(bytes) {
    const message = Object.create(null);
    message.header |= bytes[0];
    message.version |= bytes[1];
    message.sid |= bytes[2] << 8;
    message.sid |= bytes[3];
    message.length |= bytes[4] << 8;
    message.length |= bytes[5];
    message.channel |= bytes[6];
    message.status |= bytes[7];
    Object.freeze(message);
    return message;
  }
}

module.exports = MessageHandler;
