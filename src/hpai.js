class HPAI {
  constructor(host, port) {
    this.host = host;
    this.port = port;
  }

  get bytes() {
    const bytes = Buffer.allocUnsafe(4);
    const arr = this.host.split('.');
    bytes[0] = parseInt(arr[0], 10) & 255;
    bytes[1] = parseInt(arr[1], 10) & 255;
    bytes[2] = parseInt(arr[2], 10) & 255;
    bytes[3] = parseInt(arr[3], 10) & 255;
    return bytes;
  }
}

module.exports = {
  new(host, port) {
    const hpai = new HPAI(host, port);
    Object.freeze(hpai);
    return hpai;
  },
};
