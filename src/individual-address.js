const Address = require('./address');
const Exception = require('./exception');

const inRange = (a, b, c) => a <= b && b <= c;

class IndividualAddress extends Address {
  /**
   *
   * @param {number | string} address
   */
  constructor(address) {
    super(address);
    if (typeof address === 'number') {
      if (!inRange(1, address, 0xffff)) {
        throw new Exception('Individual address is out of range');
      }
      this.value = address;
      return;
    }
    const match = address.match(/^(\d+)\.(\d+)\.(\d+)$/);
    const [, aStr, bStr, cStr] = match;

    const a = parseInt(aStr, 10);
    const b = parseInt(bStr, 10);
    const c = parseInt(cStr, 10);
    if (!(inRange(0, a, 0x0f) && inRange(0, b, 0x0f) && inRange(0, c, 0xff))) {
      throw new Exception('Individual address is invalid');
    }
    this.value = ((a & 0x0f) << 12) | ((b & 0x0f) << 8) | ((c & 0xff));
    if (this.value === 0) {
      throw new Exception('Individual address with zero value is not allowed');
    }
  }

  toString() {
    return [
      (this.value >> 12) & 0x0f,
      (this.value >> 8) & 0x0f,
      (this.value) & 0xff,
    ].join('.');
  }

  get bytes() {
    const bytes = Buffer.allocUnsafe(2);
    bytes.writeUInt16BE(this.value);
    return bytes;
  }
}

module.exports = {
  unsafe(address) {
    try {
      return this.new(address);
    } catch (caughtErr) {
      return null;
    }
  },
  new(address) {
    const addr = new IndividualAddress(address);
    Object.freeze(addr);
    return addr;
  },
};
