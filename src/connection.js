/* eslint-disable no-labels */

const utils = require('./utils');
const MessageHandler = require('./message-handler');
const GroupAddress = require('./group-address-3');
const Exception = require('./exception');
const DTP = require('knx-datapoints');

class Connection extends MessageHandler {
  constructor(params) {
    super(params);
    this.seqn = null;
    this.channel = null;
    this.on('close', (closeErr) => {
      if (this.hid) {
        clearInterval(this.hid);
      }
      this.emit('disconnect', closeErr);
    });
  }

  /**
   * Opens connection
   */
  async open() {
    if (!this.socket) {
      // on connect
      await super.open();
    }
    if (this.channel) {
      // on reconnect
      const message = await this.wait('state', this.checkState);
      if (message.status === 0x00) {
        return;
      }
    }
    // if new connection is required
    const message = await this.wait('connecting', this.internalConnect);
    if (message.channel > 0x00 && message.status === 0x00) {
      this.seqn = 0;
      this.channel = message.channel;
      await this.startHearbeat();
      return;
    }
    // if new connection rejected
    throw new Exception('Connection error 0x%s', message.status.toString(16));
  }

  async ping() {
    try {
      const message = await this.wait('state', this.checkState);
      return message.status === 0x00;
    } catch (caughtErr) {
      utils.isSafeError(caughtErr);
      return false;
    }
  }

  async startHearbeat() {
    this.hid = setInterval(async () => {
      let alive = false;
      let error = null;
      try {
        alive = await this.ping();
      } catch (caughtErr) {
        utils.isSafeError(caughtErr);
        error = caughtErr;
      }
      if (!alive) {
        this.emit('disconnect', error || new Exception('Ping failed'));
      }
    }, this.config.heartbeat);
  }

  /**
   * Closes connection
   */
  async close() {
    if (this.channel) {
      try {
        await this.wait('disconnecting', this.internalDisconnect);
        this.channel = null;
        this.seqn = null;
      } catch (caughtErr) {
        /* suppress */
        utils.isSafeError(caughtErr);
      }
    }
    await super.close();
  }

  /**
   * Sends `TUNNELING_ACK`
   */
  ack(seqn, status = 0x00) {
    if (this.channel === null) {
      throw new Exception('Connection has null channel');
    }
    const bytes = Buffer.allocUnsafe(10);
    bytes[0] = 0x06;
    bytes[1] = 0x10;
    bytes[2] = 0x04;
    bytes[3] = 0x21;
    bytes[4] = 0x00;
    bytes[5] = 0x0A;
    bytes[6] = 0x04;
    bytes[7] = this.channel;
    bytes[8] = seqn;
    bytes[9] = status;
    return this.send(bytes);
  }

  /**
   * Writes data to group address with optional ack
   * @param {{ data: Buffer, address: GroupAddress, timeout?: number, ack?: GroupAddress }} params
   * @returns Object
   */
  writeRequest(params) {
    const {
      address,
      data,
      ack,
    } = params;
    let tid = null;
    let ackHandler = null;
    let resHandler = null;
    let closeHandler = null;
    const seqn = this.seqn & 0xff;
    this.seqn = this.seqn + 1;
    const ackEvent = `ack.${seqn}`;
    let resEvent = null;
    if (typeof ack === 'object') {
      resEvent = `write.${ack.value}`;
    }
    const closeEvent = 'close';
    const release = () => {
      if (tid) {
        clearTimeout(tid);
      }
      if (ackHandler) {
        this.removeListener(ackEvent, ackHandler);
      }
      if (resHandler) {
        this.removeListener(resEvent, resHandler);
      }
      if (closeHandler) {
        this.removeListener(closeEvent, closeHandler);
      }
    };
    return new Promise(async (resolve, reject) => {
      try {
        closeHandler = (closeErr) => {
          reject(closeErr || new Exception('Connection closed'));
          release();
        };
        ackHandler = (ackError) => {
          if (ackError) {
            reject(ackError);
            release();
            return;
          }
          if (typeof ack === 'object') {
            resHandler = (resError, res) => {
              if (resError) {
                reject(resError);
                release();
                return;
              }
              resolve(res);
              release();
            };
            this.on(resEvent, resHandler);
            return;
          }
          resolve();
          release();
        };
        this.on(ackEvent, ackHandler);
        this.on(closeEvent, closeHandler);
        await this.internalWrite(address, data, seqn);
        tid = setTimeout(() => {
          const timeoutErr = new Exception(
            'Timeout write bytes 0x%s to %s',
            data.toString('hex'),
            address,
          );
          reject(timeoutErr);
          release();
        }, params.timeout || this.config.timeout);
      } catch (caughtErr) {
        reject(caughtErr);
        release();
      }
    });
  }

  /**
   * Sends `TUNNELING_REQUEST`
   */
  internalWrite(address, data, seqn) {
    if (this.channel === null) {
      throw new Exception('Connection has null channel');
    }
    if (typeof data === 'number' && Number.isNaN(data)) {
      throw new Exception('Data must be uint6');
    }
    if (typeof data === 'number' && data % 1 !== 0) {
      throw new Exception('Data must be uint6');
    }
    if (typeof data === 'number' && !(0 <= data && data <= 0x3f)) {
      throw new Exception('Out of uint6 range. Use buffer instread');
    }
    if (data instanceof Buffer && data.length === 0) {
      throw new Exception('Empty buffer');
    }
    if (typeof data === 'number' || data instanceof Buffer) {
      const isUint6 = data instanceof Buffer ? false : data <= 0x3f;
      const length = 21 + (isUint6 ? 0 : data.length * data.BYTES_PER_ELEMENT);
      const bytes = Buffer.allocUnsafe(length);
      bytes[0] = 0x06;
      bytes[1] = 0x10;
      bytes[2] = 0x04;
      bytes[3] = 0x20;
      bytes[4] = (length >> 8) & 0xFF;
      bytes[5] = (length >> 0) & 0xFF;
      bytes[6] = 0x04;
      bytes[7] = this.channel;
      bytes[8] = seqn;
      bytes[9] = 0x00;
      bytes[10] = 0x11;
      bytes[11] = 0x00;
      bytes[12] = 0xAC;
      bytes[13] = 0xF0;
      bytes[14] = 0x00;
      bytes[15] = 0x00;
      bytes[16] = address.bytes[0];
      bytes[17] = address.bytes[1];
      bytes[18] = 1 + (isUint6 ? 0 : data.length);
      bytes[19] = 0x00;
      bytes[20] = 0x80;
      // eslint-disable-next-line no-restricted-syntax
      merge: {
        if (isUint6) {
          bytes[20] |= data;
          break merge;
        }
        data.copy(bytes, 21);
      }
      return this.send(bytes);
    }
    throw new Exception('Data must be of type buffer or uint6');
  }

  /**
   * Sends `TUNNELING_REQUEST`
   */
  internalRead(address, seqn) {
    if (this.channel === null) {
      throw new Exception('Connection has null channel');
    }
    const bytes = Buffer.allocUnsafe(21);
    bytes[0] = 0x06;
    bytes[1] = 0x10;
    bytes[2] = 0x04;
    bytes[3] = 0x20;
    bytes[4] = 0x00;
    bytes[5] = 0x15;
    bytes[6] = 0x04;
    bytes[7] = this.channel;
    bytes[8] = seqn;
    bytes[9] = 0x00;
    bytes[10] = 0x29;
    bytes[11] = 0x00;
    bytes[12] = 0xBC;
    bytes[13] = 0xF0;
    bytes[14] = 0x00;
    bytes[15] = 0x00;
    bytes[16] = address.bytes[0];
    bytes[17] = address.bytes[1];
    bytes[18] = 0x01;
    bytes[19] = 0x00;
    bytes[20] = 0x00;
    return this.send(bytes);
  }

  /**
   * Reads value from group address
   * @param {GroupAddress} address
   * @param {number?} timeout
   */
  readRequest(params) {
    const { address } = { ...params };
    let tid = null;
    let ackHandler = null;
    let resHandler = null;
    let closeHandler = null;
    const seqn = this.seqn & 0xff;
    this.seqn = this.seqn + 1;
    const ackEvent = `ack.${seqn}`;
    const resEvent = `write.${address.value}`;
    const closeEvent = 'close';
    const release = () => {
      if (tid) {
        clearTimeout(tid);
      }
      if (ackHandler) {
        this.removeListener(ackEvent, ackHandler);
      }
      if (resHandler) {
        this.removeListener(resEvent, resHandler);
      }
      if (closeHandler) {
        this.removeListener(closeEvent, closeHandler);
      }
    };
    return new Promise(async (resolve, reject) => {
      try {
        closeHandler = (closeErr) => {
          reject(closeErr || new Exception('Connection closed'));
          release();
        };
        ackHandler = (ackError) => {
          if (ackError) {
            reject(ackError);
            release();
            return;
          }
          resHandler = (resError, res) => {
            if (resError) {
              reject(resError);
              release();
              return;
            }
            resolve(res);
            release();
          };
          this.on(resEvent, resHandler);
        };
        this.on(ackEvent, ackHandler);
        this.on(closeEvent, closeHandler);
        await this.internalRead(address, seqn);
        tid = setTimeout(() => {
          const timeoutErr = new Exception('Timeout read from %s', address);
          reject(timeoutErr);
          release();
        }, params.timeout || this.config.timeout);
      } catch (caughtErr) {
        reject(caughtErr);
        release();
      }
    });
  }

  /**
   * Sends `CONNECT_REQUEST`
   */
  internalConnect() {
    if (this.channel !== null) {
      throw new Exception('Already connected');
    }
    const bytes = Buffer.allocUnsafe(26);
    const { local } = this.config;
    bytes[0] = 0x06;
    bytes[1] = 0x10;
    bytes[2] = 0x02;
    bytes[3] = 0x05;
    bytes[4] = 0x00;
    bytes[5] = 0x1A;
    bytes[6] = 0x08;
    bytes[7] = 0x01;
    bytes[8] = local.bytes[0];
    bytes[9] = local.bytes[1];
    bytes[10] = local.bytes[2];
    bytes[11] = local.bytes[3];
    bytes[12] = (local.port >> 8) & 255;
    bytes[13] = local.port & 255;
    bytes[14] = 0x08;
    bytes[15] = 0x01;
    bytes[16] = local.bytes[0];
    bytes[17] = local.bytes[1];
    bytes[18] = local.bytes[2];
    bytes[19] = local.bytes[3];
    bytes[20] = (local.port >> 8) & 255;
    bytes[21] = local.port & 255;
    bytes[22] = 0x04;
    bytes[23] = 0x04;
    bytes[24] = 0x02;
    bytes[25] = 0x00;
    return this.send(bytes);
  }

  /**
   * Sends `CONNECTIONSTATE_REQUEST`
   */
  checkState() {
    if (this.channel === null) {
      throw new Exception('Connection has null channel');
    }
    const { local } = this.config;
    const bytes = Buffer.allocUnsafe(16);
    bytes[0] = 0x06;
    bytes[1] = 0x10;
    bytes[2] = 0x02;
    bytes[3] = 0x07;
    bytes[4] = 0x00;
    bytes[5] = 0x10;
    bytes[6] = this.channel;
    bytes[7] = 0x00;
    bytes[8] = 0x08;
    bytes[9] = 0x01;
    bytes[10] = local.bytes[0];
    bytes[11] = local.bytes[1];
    bytes[12] = local.bytes[2];
    bytes[13] = local.bytes[3];
    bytes[14] = (local.port >> 8) & 255;
    bytes[15] = local.port & 255;
    return this.send(bytes);
  }

  /**
   * Sends `DISCONNECT_REQUEST`
   */
  internalDisconnect(channel = this.channel) {
    if (this.channel === null) {
      throw new Exception('Connection has null channel');
    }
    const { local } = this.config;
    const bytes = Buffer.allocUnsafe(16);
    bytes[0] = 0x06;
    bytes[1] = 0x10;
    bytes[2] = 0x02;
    bytes[3] = 0x09;
    bytes[4] = 0x00;
    bytes[5] = 0x10;
    bytes[6] = channel;
    bytes[7] = 0x00;
    bytes[8] = 0x08;
    bytes[9] = 0x01;
    bytes[10] = local.bytes[0];
    bytes[11] = local.bytes[1];
    bytes[12] = local.bytes[2];
    bytes[13] = local.bytes[3];
    bytes[14] = (local.port >> 8) & 255;
    bytes[15] = local.port & 255;
    return this.send(bytes);
  }

  async send(message) {
    return new Promise((resolve, reject) => {
      if (!this.socket) {
        const err = new Exception('Socket not connected');
        reject(err);
        return;
      }
      this.socket.send(
        message,
        0,
        message.length,
        this.config.server.port,
        this.config.server.host,
        (err, bytes) => {
          if (err) {
            reject(err);
            return;
          }
          if (bytes !== message.length) {
            const error = new Exception(
              'Expected to send %s bytes, but sent %s bytes',
              message.length,
              bytes,
            );
            reject(error);
            return;
          }
          resolve();
        },
      );
    });
  }

  async request(params) {
    const {
      ack,
      data,
      address,
      encoder,
      decoder,
    } = params;

    let dectype = null;
    let enctype = null;

    if (typeof data === 'undefined') {
      // read procedure
      if (typeof decoder !== 'number') {
        throw new Exception('Expected params.decoder to be DTP number');
      }
      dectype = decoder.toFixed(3);
      if (!DTP.isDptSupported(dectype)) {
        throw new Exception('Params.decoder %s is not supported', dectype);
      }
      const kwargs = { ...params };
      kwargs.address = GroupAddress.new(address);
      const { cemi } = await this.readRequest(kwargs);
      const decoded = DTP.decode(dectype, cemi);
      Object.freeze(decoded);
      return decoded;
    }

    // write procedure
    // verify params
    if (
      data === null ||
      typeof data === 'undefined' ||
      data === '') {
      throw new Exception('Invalid params.data');
    }
    if (typeof encoder !== 'number') {
      throw new Exception('Expected params.encoder to be DTP number');
    }
    enctype = encoder.toFixed(3);
    if (!DTP.isDptSupported(enctype)) {
      throw new Exception('Params.encoder %s is not supported', enctype);
    }
    if (ack) {
      if (typeof decoder !== 'number') {
        throw new Exception('Expected params.decoder to be DTP number');
      }
      dectype = decoder.toFixed(3);
      if (!DTP.isDptSupported(dectype)) {
        throw new Exception('Params.decoder %s is not supported', dectype);
      }
    }
    // prepare args
    const kwargs = { ...params };
    kwargs.address = GroupAddress.new(address);
    kwargs.data = DTP.encode(enctype, data);
    if (enctype < 4) {
      kwargs.data = kwargs.data[0];
    }
    if (ack) {
      kwargs.ack = GroupAddress.new(ack);
    }
    // write data to knxnet
    const res = await this.writeRequest(kwargs);
    if (ack) {
      // if ack provided
      const { cemi } = res;
      const decoded = DTP.decode(dectype, cemi);
      Object.freeze(decoded);
      return decoded;
    }
  }

  /**
   * Sends group read request to the bus and waits for the group response on the same address
   * Returns decoded response data
   * @param {number} decoder `DTP (x.xxx)`
   * @param {number|string} address
   * @param {number=} timeout
   * @returns {Promise<any>}
   */
  read(decoder, address, timeout) {
    return this.request({
      address,
      decoder,
      timeout,
    });
  }

  /**
   * Sends write telegram to the provided group address
   * @param {number} encoder `DTP (x.xxx)`
   * @param {number|Buffer} data `uint6` or `byte[]`
   * @param {number|string} address
   * @param {number=} timeout
   * @returns {Promise<void>}
   */
  write(encoder, data, address, timeout) {
    return this.request({
      address,
      data,
      encoder,
      timeout,
    });
  }
}

module.exports = Connection;
