const Exception = require('./exception');

class Address {
  toString() {
    throw new Exception('Address toString() is not implemented yet');
  }

  toJSON() {
    return this.toString();
  }

  get bytes() {
    throw new Exception('Address getter bytes is not implemented yet');
  }
}

module.exports = Address;
