const util = require('util');

class Exception extends Error {
  constructor(format, ...args) {
    super();
    this.name = 'KNXNetIP';
    this.message = util.format(`${format}`, ...args);
  }
}

module.exports = Exception;
