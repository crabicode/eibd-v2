module.exports = {
  sizeof(data) {
    if (data.length === 1 && data[0] < 0x3F) {
      return 1;
    }
    if (data.length === 4) {
      return 3;
    }
    return data.length + 1;
  },

  isSafeError(error, rethrow = true) {
    if (error) {
      const isSafe = !(
        error instanceof TypeError ||
        error instanceof SyntaxError ||
        error instanceof ReferenceError ||
        error instanceof EvalError
      );
      if (!isSafe && rethrow) {
        throw error;
      }
      return isSafe;
    }
    return true;
  },
};
