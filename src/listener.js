const { createSocket } = require('dgram');
const { EventEmitter } = require('events');
const HPAI = require('./hpai');

class Listener extends EventEmitter {
  constructor(params) {
    super();
    this.socket = null;
    this.setMaxListeners(256);
    this.config = {
      ...Listener.defaultSetup,
      ...params,
    };
  }

  async open() {
    return new Promise((resolve, reject) => {
      if (this.socket) {
        return;
      }
      this.config.server = HPAI.new(
        this.config.host,
        this.config.port,
      );
      this.socket = createSocket('udp4');
      this.socket.once('error', (sockErr) => {
        try {
          this.socket.close(() => {
            this.emit('close', sockErr);
            reject(sockErr);
          });
        } catch (closeErr) {
          this.emit(closeErr);
          this.emit('close', closeErr);
          reject(closeErr);
        }
      });
      this.socket.bind(() => {
        const { address, port } = this.socket.address();
        this.config.local = HPAI.new(
          address,
          port,
        );
        this.emit('open');
        resolve();
      });
    });
  }

  close() {
    return new Promise((resolve) => {
      if (this.socket) {
        try {
          this.socket.close(() => {
            this.emit('close', null);
            resolve();
          });
          return;
        } catch (caughtErr) {
          this.emit('close', null);
        }
      }
      resolve();
    });
  }
}

Listener.defaultSetup = {
  host: '224.0.23.12',
  timeout: 5000,
  heartbeat: 25000,
  port: 3671,
};

module.exports = Listener;
