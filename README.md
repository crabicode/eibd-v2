# `NodeJS` binding for `KNXNetIP`

## API

* **Acknowledged write/read requests**. This ensures delivery of sent messages to router
* **Write request may receive response** from different group address (for example: one may write to **0/0/1** and then expect some changes in **14/0/1**)
* **Read request returns values**, just like calling a function on a group address
* Support **request timeout**
* **Heartbeat mechanism** detects server downtimes
* **ES6 classes** with **async/await**
* **Safe validation** of group addresses
* **Zero dependencies**

## Limitations
* **NodeJS v7.6+**
* **No DTP deserializers** (use external lib)

## Usecase

** TODO: Add examples **
